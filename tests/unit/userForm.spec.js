import { shallowMount } from '@vue/test-utils';
import UserForm from '@/components/UserForm.vue';

describe('UserForm', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(UserForm);
  });

  it('is valid component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  describe('Labels', () => {
    it('should has label to email', () => {
      expect(wrapper.find('.form_label_email').exists()).toBe(true);
    });

    it('should has label to age', () => {
      expect(wrapper.find('.form_label_age').exists()).toBe(true);
    });
  });

  describe('Input fields', () => {
    it('should has input field to email', () => {
      expect(wrapper.find('.form_input_email').exists()).toBe(true);
    });

    it('should has input field to age', () => {
      expect(wrapper.find('.form_input_age').exists()).toBe(true);
    });

    it('should has select field to select', () => {
      expect(wrapper.find('.form_form_system').exists()).toBe(true);
    });

    describe('Validations', () => {
      beforeAll(() => {
        localStorage.setItem('email', 'email_1@gmail.com');
      });
      afterAll(() => {
        localStorage.removeItem('email');
      });

      describe('Email', () => {
        it('should has the same email used in login', () => {
          expect(wrapper.vm.hasValidEmail('email_1@gmail.com')).toBe(true);
        });

        it("should hasn't the same email used in login", () => {
          expect(wrapper.vm.hasValidEmail('email1@gmail.com')).toBe(false);
          expect(wrapper.vm.hasValidEmail('email_2@gmail.com')).toBe(false);
        });
      });

      describe('Age', () => {
        it('should be shown a message if age old is lass than 18 years', () => {
          const wrapperWithParams = shallowMount(UserForm, {
            data: () => ({ age: '17' }),
          });
          expect(wrapperWithParams.find('.form_span_age').exists()).toBe(true);
          expect(wrapperWithParams.find('.form_span_age').attributes('style')).not.toBe(
            'display: none;',
          );
        });

        it('should be shown a message if age old is over 99 years', () => {
          const wrapperWithParams = shallowMount(UserForm, {
            data: () => ({ age: '100' }),
          });
          expect(wrapperWithParams.find('.form_span_age').exists()).toBe(true);
          expect(wrapperWithParams.find('.form_span_age').attributes('style')).not.toBe(
            'display: none;',
          );
        });

        it('should not be shown a message if age is in of range 18 and 99 years', () => {
          const wrapperWithParams = shallowMount(UserForm, {
            data: () => ({ age: '50' }),
          });
          expect(wrapperWithParams.find('.form_span_age').exists()).toBe(true);
          expect(wrapperWithParams.find('.form_span_age').attributes('style')).toBe(
            'display: none;',
          );
        });
      });

      describe('Select', () => {
        it('return true if is not empty', () => {
          const wrapperWithParams = shallowMount(UserForm, {
            data: () => ({ soSelected: 'Windows' }),
          });
          expect(wrapperWithParams.vm.isValidSo).toBe(true);
        });

        it('return false if is empty', () => {
          const wrapperWithParams = shallowMount(UserForm, {
            data: () => ({ soSelected: '' }),
          });
          expect(wrapperWithParams.vm.isValidSo).toBe(false);
        });
      });

      describe('All fields', () => {
        describe('Right fills', () => {
          it('return true if email are right', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_1@gmail.com', age: '50', soSelected: 'Windows' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(true);
          });

          it('return true if age are right', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_1@gmail.com', age: '50', soSelected: 'Windows' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(true);
          });

          it('return true if system are right', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_1@gmail.com', age: '50', soSelected: 'Windows' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(true);
          });
        });

        describe('Wrong fields', () => {
          it('return false if email are wrong', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_2@gmail.com', age: '50', soSelected: 'Windows' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(false);
          });

          it('return false if select are wrong', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_1@gmail.com', age: '5', soSelected: 'Windows' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(false);
          });

          it('return false if select are wrong', () => {
            const wrapperWithParams = shallowMount(UserForm, {
              data: () => ({ email: 'email_1@gmail.com', age: '50', soSelected: '' }),
            });

            expect(wrapperWithParams.vm.allFieldsFilled).toBe(false);
          });
        });
      });
    });
  });
});

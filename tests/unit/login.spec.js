import { shallowMount } from '@vue/test-utils';
import Login from '@/components/Login.vue';

describe('Login', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Login);
  });

  it('is valid component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  describe('Input fields', () => {
    it('should has input field to email', () => {
      expect(wrapper.find('.form_input_email').exists()).toBe(true);
    });

    it('should has input field to password', () => {
      expect(wrapper.find('.form_input_password').exists()).toBe(true);
    });

    describe('Validations', () => {
      describe('Email', () => {
        it('should show a message with a valid email', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ email: 'email@email.com', password: '' }),
          });

          expect(wrapperWithParams.find('.form_span_email').text()).toBe(
            'Informe um e-mail válido',
          );
          expect(wrapperWithParams.find('.form_span_email').attributes('style')).toBe(
            'display: none;',
          );
        });

        it('should NOT show a message with a valid email', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ email: 'email.com', password: '' }),
          });

          expect(wrapperWithParams.find('.form_span_email').text()).toBe(
            'Informe um e-mail válido',
          );
          expect(wrapperWithParams.find('.form_span_email').attributes('style')).not.toBe(
            'display: none;',
          );
        });
      });
      describe('Password', () => {
        it('should show a message with a valid password', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ email: '', password: '123456' }),
          });

          expect(wrapperWithParams.find('.form_span_password').text()).toBe(
            'Informe uma senha válida',
          );
          expect(wrapperWithParams.find('.form_span_password').attributes('style')).toBe(
            'display: none;',
          );
        });

        it('should NOT show a message with a valid password', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ email: '', password: '123' }),
          });

          expect(wrapperWithParams.find('.form_span_password').text()).toBe(
            'Informe uma senha válida',
          );
          expect(wrapperWithParams.find('.form_span_password').attributes('style')).not.toBe(
            'display: none;',
          );
        });
      });
    });
  });

  describe('Methods', () => {
    describe('hasValidPassword', () => {
      it('should be true if has more or equal that six letters', () => {
        expect(wrapper.vm.hasValidPassword('123456')).toBe(true);
        expect(wrapper.vm.hasValidPassword('abcdef')).toBe(true);
      });

      it('should be false if has less that six letters', () => {
        expect(wrapper.vm.hasValidPassword('12345')).toBe(false);
        expect(wrapper.vm.hasValidPassword('abcde')).toBe(false);
      });
    });

    describe('hasValidEmail', () => {
      it('should be true if has a valid email', () => {
        expect(wrapper.vm.hasValidEmail('email@email.com')).toBe(true);
      });

      it('should be false if has a invalid email', () => {
        expect(wrapper.vm.hasValidEmail('email@email')).toBe(false);
        expect(wrapper.vm.hasValidEmail('email.com')).toBe(false);
        expect(wrapper.vm.hasValidEmail('@email.com')).toBe(false);
      });
    });

    describe('hasCorrectLogin', () => {
      it('should be a valid email and password', () => {
        expect(wrapper.vm.hasCorrectLogin('email_1@gmail.com', 'password1')).toBe(true);
      });

      it('should be a invalid email', () => {
        expect(wrapper.vm.hasCorrectLogin('email_1@asdf.com', 'password1')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('emai_1@gmail.com', 'password1')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('email_@gmail.com', 'password1')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('email@gmail.com', 'password1')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('email_1@gmailcom', 'password1')).toBe(false);
      });

      it('should be a invalid password', () => {
        expect(wrapper.vm.hasCorrectLogin('email_1@gmail.com', 'password11')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('email_1@gmail.com', 'pass1')).toBe(false);
        expect(wrapper.vm.hasCorrectLogin('email_1@gmail.com', '1')).toBe(false);
      });

      describe('Validations', () => {
        it('should be appear a message when the login is wrong', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ wrongLogin: true }),
          });
          expect(
            wrapperWithParams.find('.form_span_error_message_login').attributes('style'),
          ).not.toBe('display: none;');
        });

        it('should not be appear a message when the login is right', () => {
          const wrapperWithParams = shallowMount(Login, {
            data: () => ({ wrongLogin: false }),
          });
          expect(wrapperWithParams.find('.form_span_error_message_login').attributes('style')).toBe(
            'display: none;',
          );
        });

        describe('Redirect login', () => {
          it('should change route when click button with right login', async () => {
            const $router = {
              push: jest.fn(),
            };
            const wrapperWithParams = shallowMount(Login, {
              data: () => ({ email: 'email_100@gmail.com', password: 'password100' }),
              mocks: {
                $router,
              },
            });
            wrapperWithParams.find('.form_button_go_login').trigger('click');

            await wrapperWithParams.vm.$forceUpdate();

            expect(wrapperWithParams.vm.$router.push).toHaveBeenCalledWith({ name: 'UserForm' });
          });

          it('should change route when click button with wrong login', async () => {
            const $router = {
              push: jest.fn(),
            };
            const wrapperWithParams = shallowMount(Login, {
              data: () => ({ email: 'email_10@gmail.com', password: 'password100' }),
              mocks: {
                $router,
              },
            });
            wrapperWithParams.find('.form_button_go_login').trigger('click');

            await wrapperWithParams.vm.$forceUpdate();

            expect(wrapperWithParams.vm.$router.push).not.toHaveBeenCalledWith({
              name: 'UserForm',
            });
          });
        });
      });
    });
  });

  describe('Computed', () => {
    describe('isInvalidEmail', () => {
      it('should be true if has a valid email', () => {
        const wrapperWithParams = shallowMount(Login, {
          computed: { isInvalidEmail: () => true },
        });
        expect(wrapperWithParams.find('.form_span_email').attributes('style')).not.toBe(
          'display: none;',
        );
      });

      it('should be false if has a valid email', () => {
        const wrapperWithParams = shallowMount(Login, {
          computed: { isInvalidEmail: () => false },
        });
        expect(wrapperWithParams.find('.form_span_email').attributes('style')).toBe(
          'display: none;',
        );
      });
    });

    describe('isInvalidPassword', () => {
      it('should be true if has a valid email', () => {
        const wrapperWithParams = shallowMount(Login, {
          computed: { isInvalidPassword: () => true },
        });
        expect(wrapperWithParams.find('.form_span_password').attributes('style')).not.toBe(
          'display: none;',
        );
      });

      it('should be false if has a valid email', () => {
        const wrapperWithParams = shallowMount(Login, {
          computed: { isInvalidPassword: () => false },
        });
        expect(wrapperWithParams.find('.form_span_password').attributes('style')).toBe(
          'display: none;',
        );
      });
    });
  });
});

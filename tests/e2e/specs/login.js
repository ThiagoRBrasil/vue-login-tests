describe('Login', () => {
  const addUserForm = 'http://localhost:8080/user';
  const validEmail = 'email_1@gmail.com';
  const validPassword = 'password1';
  const localStorageToEmail = 'email';
  describe('When visit form url', () => {
    beforeEach(() => {
      cy.visit('/login');
    });

    it('contains h1 Login', () => {
      cy.contains('h1', 'Login');
    });

    describe('Login', () => {
      describe('When use a correct login', () => {
        it('should got to UserForm when click the button', () => {
          cy.get('.form_input_email').type(validEmail);
          cy.get('.form_input_password').type(validPassword);
          cy.get('.form_button_go_login')
            .click()
            .should(() => {
              expect(localStorage.getItem(localStorageToEmail)).to.eq(validEmail);
            });
          cy.url().should('eq', addUserForm);
        });

        it('should store in localStorage the email when click the button', () => {
          cy.get('.form_input_email').type(validEmail);
          cy.get('.form_input_password').type(validPassword);
          cy.get('.form_button_go_login').click();
          cy.url().should('eq', addUserForm);
        });
      });

      describe('When NOT fill corretly email and password', () => {
        it('should show incorrect message', () => {
          cy.get('.form_input_email').type('email@gmail.com');
          cy.get('.form_input_password').type('password100');
          cy.get('.form_button_go_login').click();
          cy.contains('.form_span_error_message_login', 'Login incorreto');
        });
      });
    });
  });
});

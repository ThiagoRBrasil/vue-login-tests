describe('UserForm', () => {
  const validEmail = 'email_1@gmail.com';
  const validAge = '42';
  const localStorageToEmail = 'email';
  describe('When visit form url', () => {
    beforeEach(() => {
      cy.visit('/user');
    });
    beforeEach(() => {
      localStorage.setItem(localStorageToEmail, validEmail);
    });
    afterEach(() => {
      localStorage.removeItem(localStorageToEmail);
    });

    it('should contains h1 UserForm', () => {
      cy.contains('h1', 'UserForm');
    });

    it('should contains email on localStorage', () => {
      cy.get('.form_input_email').should(() => {
        expect(localStorage.getItem(localStorageToEmail)).to.eq(validEmail);
      });
    });

    describe('Validations', () => {
      beforeEach(() => {
        cy.get('.form_input_email').clear();
        cy.get('.form_input_age').clear();
      });

      describe('When fill corretly email and idade and select some select', () => {
        it("should show 'Parabéns' message", () => {
          cy.get('.form_input_email').type(validEmail);
          cy.get('.form_input_age').type(validAge);
          cy.get('.form_form_system').select('Windows');
          cy.contains('.form_form_fields_fill', 'Parabéns');
          cy.get('.form_form_fields_fill').should('not.have.css', 'display', 'none');
        });

        it('should show a message erro with a age out of range 18 to 99', () => {
          cy.get('.form_input_age').type('17');
          cy.contains('.form_span_age', 'Informe uma idade válida');
          cy.get('.form_input_age').clear();
          cy.get('.form_input_age').type('50');
          cy.contains('.form_span_age', 'Informe uma idade válida');
        });
      });

      describe("When not fill corretly email and age and select with some option's select", () => {
        it("should NOT show 'Parabéns' message", () => {
          cy.get('.form_input_email').type('email@gmail.com');
          cy.get('.form_input_age').type('4');
          cy.get('.form_form_system').select('');
          cy.get('.form_form_fields_fill').should('have.css', 'display', 'none');
        });
      });
    });
  });
});
